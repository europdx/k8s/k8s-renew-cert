#!/bin/bash

if [[ -z $EMAIL || -z $DOMAINS || -z $SECRETNAME || -z $CUBEUSER || -z $CUBETOKEN ]]; then
	echo "EMAIL, DOMAINS, SECRETNAME, CUBEUSER and CUBETOKEN env vars required"
	env
	exit 1
fi

if [[ -n $DRYRUN ]] && [[ $DRYRUN == "TRUE" ]]; then
	SUFFIX="--dry-run"
	echo "This is dry run"
else
	SUFFIX=""
fi


echo "Inputs:"
echo " EMAIL: $EMAIL"
echo " DOMAINS: $DOMAINS"
echo " LOOP: $LOOP"
echo " SECRETNAME: $SECRETNAME"


NAMESPACE=$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace)
echo "Current Kubernetes namespce: $NAMESPACE"

sed -i "s/user-placeholder/$CUBEUSER/" ~/.kube/config
sed -i "s/token-placeholder/$CUBETOKEN/" ~/.kube/config

#cat ~/.kube/config
kubectl get ns

echo "Starting HTTP server..."
python3 -m http.server 80 &
PID=$!
echo "Starting certbot..."
certbot certonly --webroot -w / -n --agree-tos --email ${EMAIL} --no-self-upgrade -d ${DOMAINS} ${SUFFIX}

if [[ -n $LOOP ]] && [[ $LOOP == "TRUE" ]]; then
	while true; do echo 'Endless loop'; sleep 100; done
fi

kill $PID
echo "Certbot finished. Killing http server..."

echo "Finiding certs. Exiting if certs are not found ..."

CERTPATH=/etc/letsencrypt/live

if [[ -f $CERTPATH/$DOMAINS/privkey.pem ]] && [[ -f $CERTPATH/$DOMAINS/fullchain.pem ]]; then
	echo "Deleting old secret"
	kubectl delete secret $SECRETNAME -n cbio-on-demand-sandbox
	echo "Creating new secret"
	kubectl create secret tls $SECRETNAME -n cbio-on-demand-sandbox --key $CERTPATH/$DOMAINS/privkey.pem --cert $CERTPATH/$DOMAINS/fullchain.pem
else
	echo "Certs not found. Exiting ..."
	exit 1
fi
